package Zad36;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Database {
    Map<Integer,Record> dat = new HashMap<>();
    private static int counter = 0;

    public Database() {
    }
    public void addRecord(Record rc) {
        rc.setIdentyfikator(counter);
        dat.put(counter++, rc);
    }
    public void addRecord(LocalDateTime localDateTime, long timeout, String name) {
        dat.put(counter, new Record(localDateTime, timeout, counter++, name));
    }
    public void print(){
        for (Record rec:dat.values()
             ) {
            System.out.println(rec);
        }
    }
    public void refresh() {

        Iterator<Record> it = dat.values().iterator();
        while(it.hasNext()) {
            Record r = it.next();
            LocalDateTime kopiaPrzesunieta = r.getCzas_dodania().plusMinutes(r.getCzas_waznosci());
            if (LocalDateTime.now().isAfter(kopiaPrzesunieta)) {
                it.remove();
            }
        }
    }
}
