package Zad36;

//Zadanie 36:
//        Stwórz aplikację, która dodaje wpisy do rejestru, a po przedawnieniu usuwa je w obiegu ktory odswieza baze danych.
//        1. Stworz klase Record ktora reprezentuje pojedynczy wpis w bazie danych. klasa powinna posiadac:
//        -  czas dodania - data lub timestamp
//        -  czas waznosci - datę przedawnienia lub czas w milisekundach po jakim rekord ulega przedawnieniu
//        - identyfikator sprawy - liczba
//        - nazwę sprawy - tekst, moze byc jedno slowo
//        2. Stworz klase Database ktora posiada mapę rekordów Record.
//        - dodaj metodę dodawania rekordów do mapy. Rekordy powinny byc ladowane z konsoli
//        - dodaj metodę szukania rekordów po ich identyfikatorze
//        - dodaj metodę refresh() która wykonuje obieg przez wszystkie rekordy bazy danych i sprawdza czy rekord nie
// jest przedawniony. Jeśli rekord jest przedawniony powinien zostać usunięty a w konsoli powinien się pojawić odpowiedni
// komunikat.
//        Dodaj maina, w którym możesz dodawać lub odswieżać rekordy w bazie
//        https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/PrzedawnioneRekordy/src/com/amen/records/?at=master

public class Main {
    public static void main(String[] args) {

    }
}
